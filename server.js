const express = require("express");
const formidable = require("formidable");
// const cors = require("cors");

const fs = require("fs");
const path = require("path");

const app = express();
// app.use(cors);
const uploadDir = path.join(__dirname, "/uploads/");
let modelName = "";

app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
});

app.get("/", function (req, res, next) {
    res.send("We are overworking don't blame your failure on us");
});

app.post('/java', function (req, res, next) {
    var form = new formidable.IncomingForm();
    form.multiples = false;
    form.keepExtensions = true;
    form.uploadDir = uploadDir;

    form.parse(req, (err, fields, files) => {
        modelName = fields.modelName;
        modelName = modelName.charAt(0).toUpperCase() + modelName.substr(1).toLowerCase();

        if (err) return res.status(500).json({error: err});
        // res.status(200).json({uploaded: "success"})
    });

    let filePath = '';
    form.on('fileBegin', function (name, file) {
        const [fileName, fileExt] = file.name.split('.');
        filePath = `${fileName}${new Date().getDay()}.${fileExt}`;
        file.path = "uploads/"+filePath;

        setTimeout(function () {

        }, 1000);


    })
    .on('file', (name, file) => {
      res.send(digestFile(`uploads/${filePath}`));
    });

});

function digestFile(file) {
    console.log(file);
    let stream = fs.readFileSync(file, 'utf8');
    let data = stream.toString();

    let usableData = data.split(".")[1];

    usableData = usableData.split('\n');

    let tableName = usableData[0];

    fs.writeFileSync("model.java", `@Data\n@Entity\n@Table(name = ${tableName})\n`);
    fs.appendFileSync("model.java", `public class ${modelName} {\n`);

    let x = 1;

    console.log(usableData.length);
    while(x<usableData.length-1) {

        let column = usableData[x];

        if (column.search("NOT NULL") !== -1) {
            fs.appendFileSync("model.java", "\t@NotNull\n");
        }
        let regX = "DEFAULT [0-9]|DEFAULT '[a-zA-Z]{0,100}'";
        let defaults = column.match(regX);

        if (defaults !== null) {
            defaults = `, columnDefinition = "${defaults[0]}")`;
        } else {
            defaults = ")";
        }

        let nextColumn = column.match('"[a-zA-Z]{0,255}?.*_[a-zA-Z]{0,255}"');
        let columnName = '';
        if(nextColumn !== null) {
            columnName = column.match('"[a-zA-Z]{0,255}?.*_[a-zA-Z]{0,255}"')[0];
        }else{
            break;
        }
        fs.appendFileSync("model.java", `\t@Column(name = ${columnName}${defaults}\n`);

        columnName = columnName.replace(/['"]+/g, '');

        columnName = columnName.split("_");

        let name = '';
        name = columnName[1].toLowerCase();
        for(i=2; i<=columnName.length; i++){
          if(columnName[i]){
          name += columnName[i].charAt(0).toUpperCase() + columnName[i].substr(1).toLowerCase();
          }
        }

        let dataType= 'VARCHAR';
        let regX2 = "NUMBER|VARCHAR|Char|DATE|LONG";
        if(column.match(regX2) !== null) {
            dataType = column.match(regX2)[0];
        }

        dataType = convertData(dataType);

        fs.appendFileSync("model.java", `\tprivate ${dataType} ${name};\n\n`);
        ++x;
    }

    fs.appendFileSync("model.java", "}");

    return fs.readFileSync("model.java", "utf8");

}

function convertData(dataType){
    switch (dataType) {
        case 'NUMBER':
            return 'Long';
        case 'VARCHAR':
            return 'String';
        case 'Char':
            return 'String';
        case 'DATE':
            return 'Date';
            case 'LONG':
                return 'Long';
        default:
            return dataType.charAt(0).toUpperCase() + dataType.substr(1).toLowerCase();
    }
}
app.listen(1440, function () {
    console.log("Server listening on port 1440");
});
